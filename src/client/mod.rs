// Copyright © 2019 sqsquatch developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! client modules

crate mod publish;
crate mod subscribe;

use crate::error::Result;
use hyper::{client::HttpConnector, Uri};
use hyper_proxy::{Intercept, Proxy, ProxyConnector};
use hyper_tls::HttpsConnector;
use rusoto_sqs::GetQueueUrlRequest;
use std::env;

type Connector = ProxyConnector<HttpsConnector<HttpConnector>>;

fn new_connector() -> Result<Connector> {
    if let Ok(proxy_url) = env::var("http_proxy") {
        let proxy = Proxy::new(Intercept::All, proxy_url.parse::<Uri>()?);
        let connector = HttpsConnector::new(4)?;
        Ok(ProxyConnector::from_proxy(connector, proxy)?)
    } else {
        let connector = HttpsConnector::new(4)?;
        Ok(ProxyConnector::new(connector)?)
    }
}

fn get_queue_url_request(queue_name: String) -> GetQueueUrlRequest {
    GetQueueUrlRequest {
        queue_name,
        queue_owner_aws_account_id: None,
    }
}

use futures01::future::{err, ok, FutureResult};
use http::{header::HeaderName, HeaderMap, HttpTryFrom, StatusCode};
use log::info;
use rusoto_core::{
    request::HttpResponse,
    signature::{SignedRequest, SignedRequestPayload},
    ByteStream, DispatchSignedRequest, HttpDispatchError,
};
use serde::Serialize;
use std::{collections::HashMap, fmt, time::Duration};

/// Composes mock API responses
///
/// A Default is provided which returns an successful response with an empty body
///
/// These can be constructed using either a Default implementation or
///  with the [`with_status`](method.with_status) function.
///
#[derive(Default)]
pub struct MockRequestDispatcher {
    outcome: RequestOutcome,
    body: Vec<u8>,
    body_map: Option<HashMap<String, Vec<u8>>>,
    headers: HeaderMap<String>,
    request_checker: Option<Box<dyn Fn(&SignedRequest) + Send + Sync>>,
}

impl fmt::Debug for MockRequestDispatcher {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "MockRequestDispatcher {{ ... }}")
    }
}

#[allow(variant_size_differences)]
enum RequestOutcome {
    Performed(StatusCode),
    Failed(HttpDispatchError),
}

impl Default for RequestOutcome {
    fn default() -> Self {
        Self::Performed(StatusCode::default())
    }
}

#[allow(dead_code)]
impl MockRequestDispatcher {
    /// Returns a instance that mocks the status code that would
    /// be returned from AWS
    crate fn with_status(status: u16) -> Self {
        Self {
            outcome: RequestOutcome::Performed(StatusCode::try_from(status).unwrap()),
            ..Self::default()
        }
    }

    /// Mocks the service request failing with a communications error
    crate fn with_dispatch_error(error: HttpDispatchError) -> Self {
        Self {
            outcome: RequestOutcome::Failed(error),
            ..Self::default()
        }
    }

    /// Mocks the service response body what would be
    /// returned from AWS
    crate fn with_body(mut self, body: &str) -> Self {
        self.body = body.as_bytes().to_vec();
        self
    }

    /// Allows different body responses based on the action given to dispatch.
    /// This allows for mocking chained futures or multiple requests through
    /// the same dispatcher.
    #[doc(hidden)]
    #[must_use]
    pub fn with_body_map(mut self, body_map: HashMap<String, Vec<u8>>) -> Self {
        self.body_map = Some(body_map);
        self
    }

    /// Mocks the json serialized response body what would be
    /// returned from AWS
    crate fn with_json_body<B>(mut self, body: B) -> Self
    where
        B: Serialize,
    {
        self.body = serde_json::to_vec(&body).expect("failed to deserialize into json");
        self
    }

    /// Mocks the signed request checking applied to a request before sending
    /// to AWS
    crate fn with_request_checker<F>(mut self, checker: F) -> Self
    where
        F: Fn(&SignedRequest) + Send + Sync + 'static,
    {
        self.request_checker = Some(Box::new(checker));
        self
    }

    /// Mocks a single service header that would be returned from AWS
    crate fn with_header(mut self, key: &str, value: &str) -> Self {
        let _ = self
            .headers
            .insert(key.parse::<HeaderName>().unwrap(), value.into());
        self
    }
}

impl DispatchSignedRequest for MockRequestDispatcher {
    type Future = FutureResult<HttpResponse, HttpDispatchError>;

    fn dispatch(&self, request: SignedRequest, _timeout: Option<Duration>) -> Self::Future {
        if self.request_checker.is_some() {
            self.request_checker.as_ref().unwrap()(&request);
        }

        match self.outcome {
            RequestOutcome::Performed(ref status) => {
                let body = self
                    .body_map
                    .as_ref()
                    .and_then(|bm| payload_action(request).and_then(|action| bm.get(&action)))
                    .unwrap_or_else(|| &self.body)
                    .clone();
                ok(HttpResponse {
                    status: *status,
                    body: ByteStream::from(body),
                    headers: self.headers.clone(),
                })
            }
            RequestOutcome::Failed(ref error) => err(error.clone()),
        }
    }
}

fn payload_action(request: SignedRequest) -> Option<String> {
    if let Some(payload) = request.payload {
        let payload_bytes: Vec<u8> = match payload {
            SignedRequestPayload::Buffer(b) => b.to_vec(),
            SignedRequestPayload::Stream(s) => {
                use std::io::Read;
                let mut reader = s.into_blocking_read();
                let mut buf = Vec::new();
                let _ = reader.read_to_end(&mut buf).ok();
                buf
            }
        };
        let action = parse_payload_action(&String::from_utf8_lossy(&payload_bytes))?;
        info!("Payload Action: {}", action);
        Some(action)
    } else {
        None
    }
}

fn parse_payload_action(payload: &str) -> Option<String> {
    let payload_args: Vec<&str> = payload.split('&').collect();

    if payload_args.is_empty() {
        None
    } else {
        let mut result = String::new();

        for arg in payload_args {
            if arg.starts_with("Action") {
                let split: Vec<&str> = arg.split('=').collect();
                if split.len() == 2 {
                    result = split[1].to_string();
                    break;
                }
            }
        }

        if result.is_empty() {
            None
        } else {
            Some(result)
        }
    }
}
